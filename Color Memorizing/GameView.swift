//
//  File.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit


class GameView : UIViewController
{
    @IBOutlet weak var _readyLabel: UILabel!
    @IBOutlet weak var _number: UILabel!
    var gameTimer: Timer!
    var counter: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Генерируем последовательность цветов уровня
        Player.winColors = [UIImage]()
        
        for i in 0..<Player.colorCount{
            let randIndex = randomNum(8, minNum: 0)
            
            // Для проверки
            print("Цвет \(i+1) имеет индекс \(randIndex)")
            Player.winColors.append(Player.gameColors[randIndex])
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        start()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func start () {
        _number.isHidden = false
        
        UIView.animate(withDuration: 1, delay: 0, options: [] ,animations: {
            self._number.alpha = 0
            }, completion: { finished in self.animation2() })
    }
    func animation2 () {
        _number.alpha = 1
        _number.text = "2"
        UIView.animate(withDuration: 1, animations: {
            self._number.alpha = 0
            }, completion: { finished in self.animation1()})
    }
    func animation1 () {
        _number.alpha = 1
        _number.text = "1"
        UIView.animate(withDuration: 1, animations: {
            self._number.alpha = 0
            }, completion: { finished in self.finalAnimation()})
    }
    func finalAnimation () {
        self._number.text = NSLocalizedString("Поехали!", comment: "let's go!")
        UIView.animate(withDuration: 1, animations: {
            self._number.alpha = 1
            }, completion: { _ in
                self.showColors()
        })
    }
    func showColors () {
        _number.isHidden = true

        
        setBack()
        gameTimer = Timer.scheduledTimer(timeInterval: Player.sleepDuration, target: self, selector: #selector(setBack), userInfo: nil, repeats: true)
        
    }
    // Генератор случайных чисел
    func randomNum(_ maxNum: Int, minNum: Int) -> Int {
        let rand = Int(arc4random_uniform(UInt32(maxNum-minNum)) + 1) + minNum
        return rand
    }
    
    func setBack () {
         if (counter < Player.winColors.count)
         {
            self.view.backgroundColor = UIColor( patternImage: Player.winColors[self.counter])
            self.counter += 1
            Audio.makeSound()
            UIView.animate(withDuration: 0.1, delay: 0, options: [], animations: {self.view.alpha = 0}, completion: {finished in self.view.alpha = 1})
            return
         }
        gameTimer.invalidate()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "playbackView")
        self.present(vc, animated: true, completion: nil)
    }
}
