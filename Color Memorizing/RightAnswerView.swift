//
//  RightAnswerView.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

class RightAnswerView : UIViewController
{
    @IBOutlet weak var _score: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        _score.text = String(Player.currentScore)
        UIViewController.addBackground("winBackground.jpg", viewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func `continue`(_ sender: AnyObject){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "gameView")
        self.present(vc, animated: true, completion: nil)
    }
    // Выйти в меню, сохранив текущий результат
    @IBAction func showMenu(_ sender: AnyObject) {
        if (Player.currentScore > Player.highestScore)
        {
            Player.setNewHighestScore()
        }
        Player.resetScore()
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewController")
        self.present(vc, animated: true, completion: nil)
    }

}
