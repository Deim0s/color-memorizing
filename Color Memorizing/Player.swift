//
//  Player.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 14.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation // для воспроизведения музыки


class Player{
    static var highestScore: Int!
    fileprivate static let prefs = UserDefaults.standard
    static var currentScore = 0
    
    
    // Сложности в игре:
    // level | colorCount | sleepDuration
    //  1 | 3 | 2
    //  1.5 | 3 | 1.5
    //  2 | 4 | 2
    //  2 | 4 | 1.5
    //  3 | 5 | 2
    // ...
    // 10+ | 12 | 2 (1.5)
    
    static var level = 1.0
    static var colorCount: Int {
        return level >= 10 ? 12 : Int(level) + 2
    }
    static var sleepDuration: Double
    {
        return (level/0.5).truncatingRemainder(dividingBy: 2) == 1 ? 1.5 : 2
    }
    
    // Цвета, используемые в игре:
    // 0 - Красный
    // 1 - Синий
    // 2 - Зеленый
    // 3 - Желтый
    // 4 - Фиолетовый
    // 5 - Ораньжевый
    // 6 - Бирюзовый
    // 7 - Розовый
    // 8 - ТемноЗеленый
    static var gameColors: [UIImage] {
        var arr = [UIImage]()
        for i in 0..<9 {
            arr.append(UIImage(named: String(i) + ".png")!)
        }
        return arr
    }
    // Последовательность уровня
    static var winColors = [UIImage]()

    // Последовательность пользователя
    static var playerColors = [UIImage]()
    

    static func setNewHighestScore () {
        self.highestScore = self.currentScore
        
        prefs.set(highestScore, forKey: "highestScore")
        prefs.synchronize()
    }
    static func clearHighScore () {
        self.highestScore = 0
        prefs.set(highestScore, forKey: "highestScore")
        prefs.synchronize()
    }
    static func loadHighscore () {  
        self.highestScore = prefs.value(forKey: "highestScore") == nil ? 0 : prefs.value(forKey: "highestScore") as! Int
    }
    static func resetScore () {
        currentScore = 0;
        level = 1;
    }
}

