//
//  GameOverView.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

class GameOverView : UIViewController

{
    @IBOutlet weak var _score: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       UIViewController.addBackground("loseBackground.jpg", viewController: self)
        // Проверяем, побит ли рекорд
        if (Player.currentScore > Player.highestScore){
            Player.setNewHighestScore()
        }
        _score.text = String(Player.currentScore)
        Player.resetScore()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func _showMenu(_ sender: UIButton) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "viewController")
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func _playAgain(_ sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "gameView")
        self.present(vc, animated: true, completion: nil)
    }
}
