//
//  loadingScreen.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 14.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

class loadingScreen: UIViewController {
    override func viewDidLoad() {
        UIViewController.addBackground("launchImage.jpg", viewController: self)
    }
}
