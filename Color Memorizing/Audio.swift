//
//  Audio.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 28.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import Foundation
import AVFoundation // для музыки


class Audio {
    fileprivate static let colorSound = URL(fileURLWithPath: Bundle.main.path(forResource: "theSound", ofType: "wav")!)
    static var audioPlayer = AVAudioPlayer()
    static var volume: Float = 0.0
    static var isSoundOn: Bool = true
    
    
    
    static func initiateMusic () {
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: colorSound)
        }
        catch {
            
        }
        audioPlayer.prepareToPlay()
    }
    static func makeSound () {
        audioPlayer.play()
    }

}
