//
//  SettingsView.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit


class SettingsView : UIViewController
{

    @IBOutlet weak var _switcher: UISwitch!
    @IBAction func switched(_ sender: UISwitch) {
       Audio.initiateMusic()
       if (sender.isOn)
       {
           Audio.audioPlayer.volume = 1
           Audio.isSoundOn = true
       }
       else {
           Audio.audioPlayer.volume = 0
           Audio.isSoundOn = false
       }
    }
    @IBAction func _clearBestScore(_ sender: UIButton) {
        Player.clearHighScore() // Удаляет предыдущие записи о лучшем счете
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        _switcher.isOn = Audio.isSoundOn
        UIViewController.addBackground("background.jpg", viewController: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func _showMenu(_ sender: AnyObject) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "viewController")
        self.present(vc, animated: true, completion: nil)
    }
}
