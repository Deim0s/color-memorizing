//
//  Tools.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 28.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    // Устанавливает изображение с указанным путем в указанный ViewController
    static func addBackground(_ path: String, viewController: UIViewController) {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = UIImage(named: path)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        viewController.view.addSubview(imageViewBackground)
        viewController.view.sendSubview(toBack: imageViewBackground)
    }
}
