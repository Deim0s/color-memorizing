//
//  ViewController.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

class MainController: UIViewController {
    @IBOutlet weak var _highestScore: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        UIViewController.addBackground("background.jpg",viewController: self)
        
        Player.loadHighscore()
        _highestScore.text = String(Player.highestScore)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func _startGame(_ sender: UIButton) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "gameView")
        self.present(vc, animated: true, completion: nil)

    }
    @IBAction func _showSettings(_ sender: UIButton) {
        
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "settingsView")
        self.present(vc, animated: true, completion: nil)
    }


}

