//
//  PlaybackView.swift
//  Color Memorizing
//
//  Created by Evgeny Shishko on 12.07.16.
//  Copyright © 2016 Evgeny Shishko. All rights reserved.
//

import UIKit

class PlaybackView: UIViewController {
    
    @IBOutlet var _orderMarks: [UIImageView]!
    @IBOutlet var _colorButtons: [UIButton]!
    var counter = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        UIViewController.addBackground("playbackView.jpg", viewController: self)
    
        // Очищаем последовательность игрока
        Player.playerColors = [UIImage]()
        
        // Инициализируем цвета кнопок и удаляем текст:
        for i in 0..<9 {
           _colorButtons[i].setBackgroundImage(Player.gameColors[i], for: UIControlState())
           _colorButtons[i].setTitle("", for: UIControlState())
           _colorButtons[i].layer.cornerRadius = _colorButtons[i].frame.size.height / 2
           _colorButtons[i].layer.masksToBounds = true
           //_colorButtons[i].layer.borderWidth = 1
           _colorButtons[i].contentMode = UIViewContentMode.scaleAspectFill
           _colorButtons[i].tag = i
        }
        for im in _orderMarks {
           im.layer.cornerRadius = im.frame.size.height / 2
            im.layer.masksToBounds = true
            im.contentMode = UIViewContentMode.scaleAspectFill
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func add(_ sender: UIButton) {
        if (counter < _orderMarks.count)
        {
            _orderMarks[counter].image = UIImage(named: String(sender.tag) + ".png")
            _orderMarks[counter].contentMode = UIViewContentMode.scaleAspectFill
            counter = counter + 1
            Player.playerColors.append(Player.gameColors[sender.tag])
            
        }
    }
    @IBAction func cancel(_ sender: AnyObject){
        if (counter > 0)
        {
            _orderMarks[counter-1].image = nil
            Player.playerColors.removeLast()
            counter -= 1
        }
    }
    @IBAction func accept(_ sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if (Player.playerColors.elementsEqual(Player.winColors)){
            Player.currentScore += Player.colorCount// Начисление очков за правильный ответ
            Player.level += 0.5 // Повышение уровня
            let vc = storyboard.instantiateViewController(withIdentifier: "rightAnswerView")
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc = storyboard.instantiateViewController(withIdentifier: "gameOverView")
            self.present(vc, animated: true, completion: nil)
        }
    }
    

}
